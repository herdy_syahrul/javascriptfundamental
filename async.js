
// Callback

// const getName = (callback) => {
//     setTimeout(() => {
//         callback('Herdi')
//     }, 2000)
// }

// const getLastName = (callback2) => {
//     setTimeout(() => {
//         callback2('sidik')
//     }, 2000)
// }

// const getLastName2 = (callback3) => {
//     setTimeout(() => {
//         callback3('blue')
//     }, 2000)
// }


// const getFullName = () => {
//     getName((result) => {
//         getLastName((result2) => {
//             getLastName2((result3) => {
//                 const firstname = result
//                 const lastname = result2
//                 const fullname = result3
//                 const finalname = `name pertama : ${firstname} , nama kedua : ${lastname} ,
//                 nama ketiga : ${fullname}`
//                 console.log(finalname)
//             })
//         })
//     })
// }

// getFullName()

/////////////////////////////

// Promise

// const getFirstName = () => {
//     return new Promise((resolve) => {
//         setTimeout(() => {
//             resolve('Herdi')
//         }, 2000)
//     })
// }

// const secondName = (firstname) => {
//     return new Promise((resolve) => {
//         setTimeout(() => {
//             resolve(firstname + ' ' +'sidik')
//         }, 2000)
//     })
// }

// const thirdName = (second) => {
//     return new Promise((resolve) => {
//         setTimeout(() => {
//             resolve(second + ' ' +'blue')
//         }, 2000)
//     })
// }

// const fullName = () => {
//     getFirstName()
//     .then((result) => {
//         return secondName(result)
//     }).then((result2) => {
//         return thirdName(result2)
//     }).then((result3) => {
//         console.log(result3)
//     })
// }

// fullName()

const firstName = () => {
	return new Promise((resolve) => {
  	setTimeout(() => {
    	resolve('herdi')
    }, 2000)
  })
}

const secondName = () => {
	return new Promise((resolve) => {
  	setTimeout(() => {
    	resolve('sidik')
    }, 2000)
  })
}

// const fullName = () => {
//   Promise.all([firstName(), secondName()])
//   .then(([result, result1]) => {
//     const first = result
//     const last = result1
//     const full = first + last
//     console.log(full)
//   })
// }

// const fullName = async () => {
//     const result = await firstName()
//     const result1 = await secondName()
//     const full = result + result1
//     console.log(full)
// }

function Person(name, alamat, hobi){
    this.name = name
    this.alamat = alamat
    this.hobi = hobi
}

const person1 = new Person('herdy', 'Bogor', 'Baca')

const Saya = () => {
    return new Promise((resolve) => {
        setTimeout(() => {
            resolve(person1)
        }, 2000)
    })
}

const fullObject = async () => {
    const profile = await Saya()
    console.log(profile)
}

fullObject()

// fullName()
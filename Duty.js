let myPromise = new Promise((resolve,reject) => {
// dari variabel myPromise saya mengambil data dengan parameter resolve jika berhasil dan reject jika tidak
let number = 3 
// saya set variabel dengan value 3
    setTimeout(() => {  
        if(number===3){
          resolve('data valid')
        } else {
          reject('data tidak valid')
        }
       }, 3000)
    })
    // saya lakukan perkondisian dimana jika bernilai benar maka akan berisi data valid 
    // jika tidak maka data tidak valid
    // dan saya set timeout sebesar 3000ms
 const answer = () => {
    myPromise
    .then((result) => {
        console.log(result)
    })
    .catch((result) => {
        console.log(result)
    })
 }
 answer()
//  disini saya membuat variabel yang berisi function untuk mengeluarkan promise
// then berati nilai yang dihasilkan benar dan catch untuk menangkap pesan sebaliknya
// lalu saya panggil function answer untuk menjalankannya
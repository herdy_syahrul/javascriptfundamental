
var n1 = 0
var n2 = 1

function loopFibo(maxnum){

    const number = maxnum
    let nextTerm;
    
    console.log('Fibonacci Series:');
    console.log(n1); // print 0
    console.log(n2); // print 1
    
    nextTerm = n1 + n2;
    
        while (nextTerm <= number) {
    
        // print the next term
            console.log(nextTerm);
    
            n1 = n2; 
            n2 = nextTerm; 
            nextTerm = n1 + n2;

        }
    }
    
    console.log(loopFibo(20))